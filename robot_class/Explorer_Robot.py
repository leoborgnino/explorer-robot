import RPi.GPIO as GPIO
import RPi_map
import time
import threading
from scipy import misc
import numpy as np
import sys
from threading import Lock
from time import process_time
import select
import tty
import termios


sys.path.append('../mpu6050/')
sys.path.append('../rplidar/')
sys.path.append('../particle_filter/')

from Mpu6050 import Mpu6050
from Rplidar import Rplidar
from Localization import Localization

PORT_RPLIDAR = '/dev/ttyUSB0'
DIST_PP      = 50.0 # Pixels 50x50mm
DIST_PP_ENC  = 1.0/3.68
N_PARTICULAS = 30
TIME_INT0    = 0.1 # en segundos
Z_GYRO_FILTER = 1.0
Z_HIST = 5
STEP_COMP_DRIFT = 10
KP_DRIFT = 20.0
KI_DRIFT = 10.0
LOCALIZACION = True
PLOT = False
TIME_PROF = False
LOG_CONTROL = False

class Explorer_Robot ():
    """ Main class for Ingenia explorer robot.
    
    """
    def __init__ (self, map_raw, mov_commands=None):
        """ Explorer Robot Constructor:
        1. Init MPU6050 (Acel+Gyro)
        1. Init Self test

        Args: map_raw: mapa o grid del entorno
              mov_commands: para usar en modo PlanningLess

        """

        if (mov_commands != None):
            self.mov_commands = mov_commands

        # Clase de adquisicion y configuracion del acelerometro
        self.mpu6050 = Mpu6050()

        # Funcion de procesamiento del mapa
        mapa_env, obstaculos, init, goal = self.process_map(map_raw)

        # Clase y variable de adquisicion y configuracion
        # del laser
        if ( LOCALIZACION == True ):
            self.scan_data_lock = Lock()
            self.rplidar = Rplidar( PORT_RPLIDAR, self.scan_data_lock )
            # Clase localizacion, internamente tiene el filtro de particulas
            self.localization = Localization(mapa_env, obstaculos, N_PARTICULAS, DIST_PP, PLOT, TIME_PROF)

        # Variables de movimiento
        self.rotating = False
        self.rotacion = 0.0
        self.rotacion_target = 0.0
        self.int_drift_p = 0.0

        self.distancia_tmp = 0.0
        self.distancia_step = 0.0
        self.distancia_target = 0.0

        self.vel_inst  = 0.0
        self.vel_motor_A  = 0.0
        self.vel_motor_B  = 0.0

        self.forward = True

        self.moving = False
        self.time_thread = threading.Timer(TIME_INT0,self.timer_interrupt)
        self.time_thread.start()

        # Log control
        if ( LOG_CONTROL == True ):
            self.log_control_file = open('output/control_file.log','w')
            self.log_control_file.write("Time \t\t DTarget \t\t DTmp \t\t Rot \t\t RotTarget \t\t ZTmp \t\t VelMotorA \t\t VelMotorB \t\t FlagMoving \t\t DriftComp \t\t IDrift \n")

        # Configuracion de la Raspberry
        GPIO.setmode(GPIO.BOARD)
        GPIO.setwarnings(False)        
        self.set_inputs()
        self.set_outputs()
        #self.self_test()

        # Funcionamiento Principal
        self.mission()
    
    def mission(self):
        if (self.mov_commands != None): ## Planning-less mode
            for i in self.mov_commands:

                if ( LOCALIZACION == True ):
                    if ( TIME_PROF == True ):
                        t1_predict_start = process_time()
                        
                    self.localization.predict(i)
                    
                    if ( TIME_PROF == True ):
                        t1_predict_end = process_time()
                        print("Tiempo de prediccion: %f"%(t1_predict_end-t1_predict_start) )

                stop = False
                self.rotate(i[1])
                with NonBlockingConsole() as nbc:
                    while ( self.rotating == True and stop == False):
                        if nbc.get_data() == '\x1b':  # x1b is ESC
                            stop = True
                            break

                stop = False
                if (i[0] > 0 ):
                    self.move(i[0],i[2])
                with NonBlockingConsole() as nbc:
                    while ( self.moving == True and stop == False):
                        if nbc.get_data() == '\x1b':  # x1b is ESC
                            stop = True
                            break

                time.sleep(0.5)
                print("Get SCAN")
                if ( LOCALIZACION == True ):
                    while (self.scan_data_lock.locked() == True or self.rplidar.get_data_cartesian() == [] ):
                        pass
                    data_lidar = self.rplidar.get_data_cartesian()
                    if ( TIME_PROF == True ):
                        t1_update_start = process_time()
                    self.localization.update(data_lidar)
                    if ( TIME_PROF == True ):
                        t1_update_end = process_time()
                        print("Tiempo de update top: %f"%(t1_update_end-t1_update_start) )
            #input("Press Enter to continue...")

    ''' Funciones de testeo de los elementos electronicos '''
    def self_test(self):
        """ This function tests the sensors and
            motors of explorer robot """
        error = 0
        print("****************************")
        print("****     SELF TEST     *****")

        self.move(10,50,0)
        time.sleep(2)
        
        if (self.distancia_tmp > 0):
            print("Motor:      OK")
            print (self.distancia_tmp)
        else:
            print("Motor:      ERROR")
            error = 1
        self.move(-10,40,0)
        time.sleep(2)
            
        # Accel get data basic test
        if ( self.mpu6050.get_accel()[2] == 0.0 ):
            print("Accel data: ERROR")
            error = 1
        else:
            print("Accel data: OK")

        if ( LOCALIZACION == True ):
            if (self.scan_data_lock.locked() == False):
                if ( self.rplidar.get_data()[0] == 0.0 ):
                    print("RPlidar:    ERROR")
                else:
                    print("RPlidar:    OK")            
        print("****************************")
        return error

    ''' Funciones Extra '''
    def process_map (self, map_raw_path):
        m_imagen = misc.imread(map_raw_path)
        obstaculos = []
        m_plan = np.zeros((len(m_imagen),len(m_imagen[0])))
        init = [0,0]
        goal = [len(m_imagen)-1,len(m_imagen[0])-1]
        for i in range(len(m_imagen)):
            for j in range(len(m_imagen[0])):
                if (m_imagen[i][j][0] > 200) and (m_imagen[i][j][1] > 200) and (m_imagen[i][j][2] > 200):
                    m_plan[i][j] = 0
                elif (m_imagen[i][j][0] < 200) and (m_imagen[i][j][1] < 200) and (m_imagen[i][j][2] < 200):
                    obstaculos.append([len(m_imagen)-1-i,j])
                    m_plan[i][j] = 1
                elif (m_imagen[i][j][0] > 200) and (m_imagen[i][j][1] < 100) and (m_imagen[i][j][2] < 100):
                    goal = [i,j]
                elif (m_imagen[i][j][0] < 100) and (m_imagen[i][j][1] > 200) and (m_imagen[i][j][2] < 100):
                    init = [i,j]
        for i in range(len(m_plan)):
            print(m_plan[i])
        print("Inicio: %d %d" % (init[0],init[1]))
        print("Final:  %d %d" % (goal[0],goal[1]))
        return (m_plan,obstaculos,init,goal)

    '''
    Funciones de movimiento
    '''
    def rotate(self, orientation):
        self.rotacion_target = orientation
        self.vel_motor_A = float(55)
        self.vel_motor_B = float(55)
        
        if ( (self.rotacion_target - self.rotacion) < 0 ):
            GPIO.output(RPi_map.CLKWA0,1)
            GPIO.output(RPi_map.CLKWA1,0)        
            GPIO.output(RPi_map.CLKWB0,1)
            GPIO.output(RPi_map.CLKWB1,0)
        else:
            GPIO.output(RPi_map.CLKWA0,0)
            GPIO.output(RPi_map.CLKWA1,1)        
            GPIO.output(RPi_map.CLKWB0,0)
            GPIO.output(RPi_map.CLKWB1,1)

        self.pwm_A.start(self.vel_motor_A)
        self.pwm_B.start(self.vel_motor_B)

        self.rotating = True

    
    def move(self, distance, velocity):
        self.distancia_tmp = 0.0
        self.distancia_target = distance
        self.vel_motor_A = float(velocity)
        self.vel_motor_B = float(velocity)

        # Motor basic test
        if ( distance > 0 ):
            GPIO.output(RPi_map.CLKWA0,1)
            GPIO.output(RPi_map.CLKWA1,0)        
            GPIO.output(RPi_map.CLKWB0,0)
            GPIO.output(RPi_map.CLKWB1,1)
            self.forward = True
        else:
            GPIO.output(RPi_map.CLKWA0,0)
            GPIO.output(RPi_map.CLKWA1,1)        
            GPIO.output(RPi_map.CLKWB0,1)
            GPIO.output(RPi_map.CLKWB1,0)
            self.forward = False

        self.pwm_A.start(self.vel_motor_A)
        self.pwm_B.start(self.vel_motor_B)

        self.moving = True

    def stop(self):
        self.pwm_A.start(0.0)
        self.pwm_B.start(0.0)

        self.moving = False
        self.rotating = False

    '''
    Funciones de hardware
    '''
    def set_inputs(self):
        """ This function sets the TRIS for GPIO
        inputs (Encoders)
        """
        GPIO.setup(RPi_map.ENCODER_A0, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.setup(RPi_map.ENCODER_A1, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.setup(RPi_map.ENCODER_B0, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.setup(RPi_map.ENCODER_B1, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        # Interrupts
        GPIO.add_event_detect(RPi_map.ENCODER_B0, GPIO.RISING, callback=self.ext_int_encoder )  

    def set_outputs(self):
        """ This function sets the TRIS for GPIO
        outputs (Motors)
        """
        GPIO.setup(RPi_map.PWMA  , GPIO.OUT)
        GPIO.setup(RPi_map.CLKWA0, GPIO.OUT)
        GPIO.setup(RPi_map.CLKWA1, GPIO.OUT)
        GPIO.setup(RPi_map.PWMB  , GPIO.OUT)
        GPIO.setup(RPi_map.CLKWB0, GPIO.OUT)
        GPIO.setup(RPi_map.CLKWB1, GPIO.OUT)
        self.pwm_A = GPIO.PWM(RPi_map.PWMA, 500)
        self.pwm_B = GPIO.PWM(RPi_map.PWMB, 500)

    def ext_int_encoder(self,channel):
        """ Interrupcion externa
            Cada pulso proveniente del encoder indica 
            una distancia recorrida
        """
        if (GPIO.input(RPi_map.ENCODER_B1) == 1):
            self.distancia_tmp += DIST_PP_ENC
        else:
            self.distancia_tmp -= DIST_PP_ENC

    def timer_interrupt(self):
        # Movimiento de traslacion
        # print("DISTANCIA TARGET %f"%self.distancia_target)
        # print("DISTANCIA TMP %f"%self.distancia_tmp)
        if ( self.moving == True ):
             if ( ( (self.distancia_target >= 0) and (self.distancia_target <= self.distancia_tmp) ) or \
                  ( (self.distancia_target < 0 ) and (self.distancia_target >= self.distancia_tmp) ) ):
                 self.stop()
        # Rotacion
        # print("Rotacion Temp: %f"%gyro[2])
        # print("Rotacion accum: %f"%self.rotacion)
        gyro = self.mpu6050.get_gyro()
        if (abs(gyro[2]) > Z_GYRO_FILTER ): # FIXME: Filtra el cambio de grados chicos, puede llevar a drifs
            self.rotacion += gyro[2] #* TIME_INT0

        if ( self.rotating == True ):
             if ( abs(self.rotacion - self.rotacion_target) < Z_HIST  ):
                 self.stop()

        if ( LOG_CONTROL == True ):
            self.log_control_file.write("%f %f %f %f %f %f %f %f %f\n"%(process_time(),self.distancia_target,self.distancia_tmp,self.rotacion,self.rotacion_target,gyro[2],self.vel_motor_A,self.vel_motor_B,self.moving))
                 
        self.time_thread.cancel()
        self.time_thread = threading.Timer(TIME_INT0,self.timer_interrupt)
        self.time_thread.start()

    def cleanup (self):
        print('Cleanup Explorer Robot')
        self.pwm_A.start(0)
        self.pwm_B.start(0)
        if ( LOCALIZACION == True ):
            self.rplidar.cleanup()
        self.time_thread.cancel()
        GPIO.remove_event_detect(RPi_map.ENCODER_B0)
        GPIO.remove_event_detect(RPi_map.ENCODER_B1)
        GPIO.remove_event_detect(RPi_map.ENCODER_A0)
        GPIO.remove_event_detect(RPi_map.ENCODER_A1)
        GPIO.cleanup()

class NonBlockingConsole(object):

    def __enter__(self):
        self.old_settings = termios.tcgetattr(sys.stdin)
        tty.setcbreak(sys.stdin.fileno())
        return self

    def __exit__(self, type, value, traceback):
        termios.tcsetattr(sys.stdin, termios.TCSADRAIN, self.old_settings)


    def get_data(self):
        if select.select([sys.stdin], [], [], 0) == ([sys.stdin], [], []):
            return sys.stdin.read(1)
        return False

