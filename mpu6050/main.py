from Mpu6050 import Mpu6050

mpu6050 = Mpu6050()

print(mpu6050.get_accel())
print(mpu6050.get_gyro())
print(mpu6050.get_temp())
