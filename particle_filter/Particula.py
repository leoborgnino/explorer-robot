# Clase que simula un robot/particula, su posicion (x,y,theta),
# su movimiento, ruido en el sensado y movimiento.

from math import *
import random
import matplotlib.pyplot as plt
from time import process_time
from sklearn.neighbors import KDTree
import numpy as np

class Particula ():
    
    def __init__ ( self, world_limit, obstaculos, scale_map, id, plot, time_prof ):
        self.id = id
        self.world_limit = world_limit
        self.obstaculos  = obstaculos
        self.forward_noise = 0.05
        self.turn_noise = 0.00
        self.sense_noise = 0.1
        # posicion inicial (x,y,theta)
        self.plot_enable = plot
        self.time_prof = time_prof
        self.plot_step = 0
        self.scale_map = scale_map # Cuanto vale cada pixel en la realidad
        self.sensor_range = 5000; # mm
        self.x = random.random() * (self.world_limit[0]*scale_map)
        self.y = random.random() * (self.world_limit[1]*scale_map)
        self.weight = 1.0
        self.orientation = random.random() * 2.0 * pi
        self.scale_dist = 500.0
        
    # Setear el ruido en el movimiento de traslacion, de rotacion de la prediccion
    def set_noise(self, new_forward_noise, new_turn_noise, new_sense_noise ):
        self.forward_noise = float(new_forward_noise)
        self.turn_noise = float(new_turn_noise)
        self.sense_noise = float(new_sense_noise)

    # Setear una nueva posicion de la particula (x,y,theta)
    def set(self, new_x, new_y, new_orientation):
        if new_x < 0 or new_x >= self.world_limit[0]*self.scale_map:
            raise ValueError('X fuera del mapa')
        if new_y < 0 or new_y >= self.world_limit[1]*self.scale_map:
            raise ValueError('Y fuera del mapa')
        if new_orientation < 0 or new_orientation >= 3 * pi:
            raise ValueError('Orientacion incorrecta')
        self.x = float(new_x)
        self.y = float(new_y)
        self.orientation = float(new_orientation)

    # Movimiento del robot, parametros giro y traslacion
    def move(self, turn, forward):
        if forward < 0:
            raise ValueError('El robot no puede moverse hacia atras')

        # Orientacion theta: a la orientacion del robot se le suma el nuevo giro mas ruido
        # gaussiano aditivo
        
        orientation = self.orientation + float(turn*(pi/180)) + random.gauss(0.0, self.turn_noise)
        orientation %= 3 * pi

        # Traslacion x,y: a la posicion del robot se le suma la nueva traslacion
        # mas ruido gaussiando aditivo
        dist = float(forward*10.0) + random.gauss(0.0, self.forward_noise)
        x = self.x + (cos(orientation) * dist)
        y = self.y + (sin(orientation) * dist)
        x %= (self.world_limit[0]*self.scale_map)
        y %= (self.world_limit[1]*self.scale_map)

        # Retorna una particula con la posicion y orientacion
        # fruto del movimiento realizado
        self.set(x, y, orientation)

    @staticmethod
    def gaussian(mu, sigma, x):
        # Funcion del ruido
        return exp(- ((mu - x) ** 2) / (sigma ** 2) / 2.0) / sqrt(2.0 * pi * (sigma ** 2))

    # Obtenemos la probabilidad o peso de la particula en si misma
    # de acuerdo a las distancias con los obstaculos y las mediciones
    def update(self, mediciones):
        # 1: Transformacion de Coordenadas 
        # Coordenadas de la particula a coordenadas del mapa
        #print("Orientacion: %f"%self.orientation)
        #print(mediciones)
        if (self.id == 1 and self.time_prof == True):
            t_rot_s = process_time()
        obs_converted = []
        for i in mediciones:
            x = self.x + cos(self.orientation) * i[0] - sin(self.orientation) * i[1]
            y = self.y + sin(self.orientation) * i[0] + cos(self.orientation) * i[1]
            obs_converted.append([x,y])

        obs_converted_np = np.asarray(obs_converted)
        obs_converted_tree = KDTree(obs_converted_np,leaf_size=2)
            
        if (self.id == 1 and self.time_prof == True):
            t_rot_e = process_time()
            print ("-- Tiempo de Rotacion: %f"%(t_rot_e-t_rot_s))

        # 2: Prediccion de la medicion 
        # Tomar los landmark que veria la particula segun su posicion
        # desde el set de obstaculos a priori
        if (self.id == 1 and self.time_prof == True):
            t_pred_s = process_time()
        obs_predicted = []
        for i in self.obstaculos:
            if ( abs(i[0]*self.scale_map-self.x) <= self.sensor_range and abs(i[1]*self.scale_map-self.y) <= self.sensor_range ): # Esto en el plano es un cuadrado, probar con distancia euclidea que seria un circulo (deberia andar mas lento pero mas preciso)
                obs_predicted.append([(i[0])*self.scale_map,(i[1])*self.scale_map])
        if (self.id == 1 and self.time_prof == True):
            t_pred_e = process_time()
            print ("-- Tiempo de PrediccionLnmk: %f"%(t_pred_e-t_pred_s))

        # 3: Asociacion de datos
        # Usando nearest neighbour asociamos cada observacion
        # a un landmark esperado
        if (self.id == 1 and self.time_prof == True):
            t_asoc_s = process_time()

        dist_landmarks = []
            
        for i in range(len(obs_predicted)):
            nearest_dist, nearest_ind = obs_converted_tree.query(np.asarray(obs_predicted[i]).reshape(1, -1), k=1) # Podria intentarse eliminar el punto que tomamos o rebuildear el arbol sin ese punto
            dist_landmarks.append(nearest_dist[0]/self.scale_dist)
            
        # for i in range(len(obs_predicted)):
        #     dist_min_temp = self.sensor_range
        #     for j in range(len(obs_converted)):
        #         dist_temp = sqrt((obs_converted[j][0] - obs_predicted[i][0]) ** 2 + (obs_converted[j][1] - obs_predicted[i][1]) ** 2)
        #         if (dist_min_temp > dist_temp):
        #             id_selected = j
        #             dist_min_temp = dist_temp
        #     id_array.append(id_selected) # En este arreglo estan las distancias minimas de cada obstaculo medido en la prediccion
        if (self.id == 1 and self.time_prof == True ):
            t_asoc_e = process_time()
            print ("-- Tiempo de AsociccionLnmk: %f"%(t_asoc_e-t_asoc_s))

        #for i in range(len(obs_predicted)):
        #    print(obs_converted[id_array[i]])
        #    print(obs_predicted[i])
        #print(id_array)
            
        # 4: Calculo de pesos
        # Por cada observacion zt aplicamos la gaussiana y luego multiplicamos todas
        if (self.id == 1 and self.time_prof == True):
            t_weight_s = process_time()
        eta = (1.0/(2.0 * pi * self.sense_noise * self.sense_noise)); # FIXME desviacion igual para x e y

        #print(eta)        
        prob = 1.0 # max 3
        for i in range(len(obs_predicted)):
            prob *= exp (-dist_landmarks[i])
            #prob *= exp ( - ( ((obs_converted[id_array[i]][0] - obs_predicted[i][0])/self.scale_dist) ** 2 + ((obs_converted[id_array[i]][1] - obs_predicted[i][1])/self.scale_dist) ** 2 ) )
            #prob *= exp ( - ( ((obs_converted[id_array[i]][0] - obs_predicted[i][0])/self.scale_dist) ** 2 + ((obs_converted[id_array[i]][1] - obs_predicted[i][1])/self.scale_dist) ** 2 ) )
            ##prob *= exp ( - ( ((obs_converted[id_array[i]][0] - obs_predicted[i][0])/self.scale_dist) ** 2 + ((obs_converted[id_array[i]][1] - obs_predicted[i][1])/self.scale_dist) ** 2 ) )
            ##print(( ((obs_converted[id_array[i]][0] - obs_predicted[i][0])/500.0) ** 2 + ((obs_converted[id_array[i]][1] - obs_predicted[i][1])/500.0) ** 2 ))
            ##print(exp ( - ( ((obs_converted[id_array[i]][0] - obs_predicted[i][0])/500.0) ** 2 + ((obs_converted[id_array[i]][1] - obs_predicted[i][1])/500.0) ** 2 ) ) )

        self.weight = prob
        #print("Prob: %f"%prob)
        if (self.id == 1 and self.time_prof == True ):
            t_weight_e = process_time()
            print ("-- Tiempo de WeightLnmk: %f"%(t_weight_e-t_weight_s))

        # Plot Debug
        if ( self.plot_enable == True and False):
            print (self.obstaculos)
            plt.figure("Obstaculos vs Obstaculos Convertidos", figsize=(10., 10.))
            x = []
            y = []
            for i in mediciones:
                x.append(i[0])
                y.append(i[1])
            plt.plot(x,y,'bo')
            x = []
            y = []
            for i in obs_converted:
                x.append(i[0])
                y.append(i[1])
            plt.plot(x,y,'ro')            
            x = []
            y = []
            for i in obs_predicted:
                x.append(i[0])
                y.append(i[1])
            plt.plot(x,y,'co')
            #plt.show()
            plt.savefig("output/observations_" + str(self.id) + "_" + str(self.plot_step) + ".png")
            plt.close()
            self.plot_step += 1            

    # Describe el objeto/particula en cuestion
    def __repr__(self):
        return '[x=%.6s y=%.6s orient=%.6s]' % (str(self.x), str(self.y), str(self.orientation))

    def get_weight(self):
        return self.weight

    def get_state(self):
        return (self.x,self.y,self.orientation)

    def set_weight(self, weight):
        self.weight = weight
