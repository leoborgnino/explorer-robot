from math import *
import matplotlib.pyplot as plt

def visualization(robot, step, p, pr, weights, world_size_x,world_size_y, obstacles, scale_map, identificador):
    step = step + 1
    print(step)
    plt.figure("Mapa del Robot", figsize=(10., 10.))
    plt.title('Etapa de ' + identificador + ' del Robot Iteracion ' + str(step))

    grid = [0, world_size_x, 0, world_size_y]
    plt.axis(grid)
    plt.grid(b=True, which='major', color='0.75', linestyle='--')
    plt.xticks([i for i in range(0, int(world_size_x), 5)])
    plt.yticks([i for i in range(0, int(world_size_y), 5)])

    for ind in range(len(pr)):
        circle = plt.Circle((pr[ind].x/scale_map, pr[ind].y/scale_map), pr[ind].get_weight()*1.0, facecolor='#0033cc', edgecolor='#0033cc', alpha=0.5)
        plt.gca().add_patch(circle)
        arrow = plt.Arrow(pr[ind].x/scale_map, pr[ind].y/scale_map, pr[ind].get_weight()*1.0*cos(pr[ind].orientation), pr[ind].get_weight()*1.0*sin(pr[ind].orientation), alpha=2., facecolor='#ffffff', edgecolor='#0033cc')
        plt.gca().add_patch(arrow)
        plt.text(pr[ind].x/scale_map, pr[ind].y/scale_map,str(round(weights[ind],2)),fontsize=12)
        #plt.gca().add_patch(text)

    # Obtaculos
    for lm in obstacles:
        circle = plt.Circle((lm[0], lm[1]), 0.25, facecolor='#1a0500', edgecolor='#000000')
        plt.gca().add_patch(circle)

    ## Robot
    #circle = plt.Circle((robot.x/scale_map, robot.y/scale_map), 0.25, facecolor='#ccff33', edgecolor='#ffffff')
    #plt.gca().add_patch(circle)
    #
    ## Orientacion Robot
    #arrow = plt.Arrow(robot.x/scale_map, robot.y/scale_map, 0.5*cos(robot.orientation), 0.5*sin(robot.orientation), alpha=0.8, facecolor='#ffffff', edgecolor='#000000')
    #plt.gca().add_patch(arrow)
    
    plt.savefig("output/" + ("%02d" %step) + identificador + ("%02d" %step) + ".png")
    plt.close()
