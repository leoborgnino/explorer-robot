import numpy as np
import random
from math import *
import matplotlib.pyplot as plt
import time
from scipy import misc
import Particula
import PlotClass
import copy
from time import process_time

class Localization():

    def __init__(self, mapa_env, obstaculos, n_particulas, scale_map, plot, time_prof):
        self.n_particulas = n_particulas
        self.obstaculos   = obstaculos
        self.map_env      = mapa_env
        self.particulas   = []
        self.world_limit  = [len(mapa_env),len(mapa_env[0])]
        self.iter_cnt = 0
        self.scale_map = scale_map
        self.log_enable = False
        self.plot_enable = plot
        self.time_prof = time_prof

        if ( self.log_enable == True ):
            file_obstaculos = open('output/obstaculos.log','w')
            for i in obstaculos:
                file_obstaculos.write("%d %d\n"%(i[0],i[1]))
            file_obstaculos.close()

            file_map = open('output/map.log','w')
            for i in range(len(mapa_env)):
                for j in range(len(mapa_env[0])):
                    file_map.write("%d "%mapa_env[i][j])
                file_map.write("\n")
            file_map.close()

            self.file_particles = open('output/particle_data.log','w')
            #self.file_particles.write("Iter | ID | x | y | theta | prob \n")

            self.file_mediciones = open('output/mediciones.log','w')

        self.weights = []
        # Particle instances
        for i in range(self.n_particulas):
            particle = Particula.Particula( self.world_limit, self.obstaculos, self.scale_map, i, self.plot_enable, self.time_prof )
            particle.set_noise( 20.0, 0.2, 0.1 )
            self.particulas.append(particle)
            self.weights.append(0.0)
        print ("Array de particulas = ", self.particulas)

    def predict(self,move):
        # Etapa de Prediccion
        for i in range(len(self.particulas)):
            self.particulas[i].move(move[1], move[0])
        if (self.plot_enable):
            # Plot
            PlotClass.visualization(self.particulas[0], self.iter_cnt, self.particulas, self.particulas, self.weights, self.world_limit[0],self.world_limit[1], self.obstaculos, self.scale_map, '1Prediccion')

    def update(self,scan):
        # Etapa de actualizacion
        if ( self.time_prof == True ):
            t_update_s = process_time()
        for i in range(len(self.particulas)):
            self.particulas[i].update(scan)
        if ( self.time_prof == True ):
            t_update_e = process_time()
            print("- Tiempo de update: %f"%(t_update_e-t_update_s) )

        # Log mediciones
        if ( self.log_enable == True ):
            for i in scan:
                self.file_mediciones.write("%d %.3f %.3f\n"%(self.iter_cnt,i[0],i[1]))

        # Normalizacion
        if ( self.time_prof == True ):
            t_norm_s = process_time()
        weights_accum = 0
        for i in range(len(self.particulas)): # FIXME: esto no tiene que existir
            weight_tmp = self.particulas[i].get_weight()
            if ( self.log_enable == True ):
                state_part = self.particulas[i].get_state()
                self.file_particles.write("%d %d %.3f %.3f %.3f %.3f \n"%(self.iter_cnt,i,state_part[0],state_part[1],state_part[2],weight_tmp))
            weights_accum += weight_tmp

        self.weights = []
        for i in range(len(self.particulas)):
            self.particulas[i].set_weight( self.particulas[i].get_weight() / weights_accum )
            self.weights.append(self.particulas[i].get_weight())
        if ( self.time_prof == True ):
            t_norm_e = process_time()
            print("- Tiempo de Norm: %f"%(t_norm_e-t_norm_s) )

        if (self.plot_enable):
            plt.stem(self.weights)
            plt.savefig("output/weights_" + str(self.iter_cnt) + ".png")
            plt.close()
    
            # Plot
            PlotClass.visualization(self.particulas[0], self.iter_cnt, self.particulas, self.particulas, self.weights, self.world_limit[0],self.world_limit[1], self.obstaculos, self.scale_map, '2Pesos')

        # Resampleado
        if ( self.time_prof == True ):
            t_res_s = process_time()
        self.resample()
        if ( self.time_prof == True ):
            t_res_e = process_time()
            print("- Tiempo de Resample: %f"%(t_res_e-t_res_s) )

        if (self.plot_enable):
            # Plot
            PlotClass.visualization(self.particulas[0], self.iter_cnt, self.particulas, self.particulas, self.weights, self.world_limit[0],self.world_limit[1], self.obstaculos, self.scale_map, '0Resample')
            self.iter_cnt += 1
        
        #print ("Array de particulas = ", self.particulas)

    def resample(self):        
        p3 = []
        c_sum = 0.0
        # Particula con mayor peso
        particula_mayor_peso = 0.0
        for i in range(len(self.particulas)):
            weight_tmp = self.particulas[i].get_weight()
            if ( weight_tmp > particula_mayor_peso):
                particula_mayor_peso = weight_tmp
        
        # Escalamiento respecto a la particula de mayo peso
        particulas_new = []
        index = int(random.random() * len(self.particulas))
        for i in range(len(self.particulas)):
            c_sum += random.random() * particula_mayor_peso
            while c_sum > self.particulas[index].get_weight():
                c_sum -= self.particulas[index].get_weight()
                index = (index + 1) % len(self.particulas)
            particulas_new.append(copy.deepcopy(self.particulas[index]))
        self.particulas = particulas_new

